<?php

/**
 * @file
 * The admin settings for the 'Force 404' module.
 */

/**
 * Implements hook_settings().
 */
function force404_admin_settings() {
  $form['text'] = array(
    '#type' => 'markup',
    '#markup' => '<p>' . t('A 404 is only forced for <strong>node</strong> URLs (aliased or not) containing a valid parent path.') . '</p>',
  );
  $form['force404_arg1'] = array(
    '#type' => 'textfield',
    '#title' => t('What URLs to exclude based on the path (second argument)'),
    '#default_value' => variable_get('force404_arg1', 'add'),
    '#description' => t('Enter a comma seperated list.'),
    '#field_prefix' => 'http(s)://(www.)example.com/node/',
  );
  $form['force404_arg2'] = array(
    '#type' => 'textfield',
    '#title' => t('What URLs to exclude based on the path (third argument)'),
    '#default_value' => variable_get('force404_arg2', 'webform'),
    '#description' => t('Enter a comma seperated list.'),
    '#field_prefix' => 'http(s)://(www.)example.com/node/123/',
  );

  // Call submit_function() on form submission.
  $form['#submit'][] = 'force404_admin_settings_submit';

  return system_settings_form($form);
}

/**
 * Validate submission.
 */
function force404_admin_settings_validate($form, &$form_state) {
}

/**
 * Submit form data.
 */
function force404_admin_settings_submit($form, &$form_state) {
}
