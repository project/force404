-- SUMMARY --

Any non-existant URL containing a valid parent path loads the parent page
returning a 200 instead of a 404. See https://www.drupal.org/node/432384. While
solved for Drupal 8 it is still an issue for Drupal 7. This module checks on
node pages (aliased or not) if the current URL is a "known" router item using
menu_get_item(). If not it returns a 404.

Some contrib modules might use node paths that are not in the menu_router table
and therefore create false positives. Their path pattern can be excluded through
the module settings by defining path arguments on which the 404s should be
avoided.

If there is a redirect defined (https://www.drupal.org/project/redirect) for the
current page a 404 will also be skipped.


-- CONFIGURATION --

Define the node path arguments to exclude at
../admin/config/search/force404.
